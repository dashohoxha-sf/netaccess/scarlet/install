#!/bin/bash

if [ "$1" = "" ]
then
  echo "Usage: $0 initrd-file"
  exit
fi
initrd_file=$1

### copy initrd.img, unzip and mount it
cp $initrd_file initrd-img.gz
gunzip initrd-img.gz
mkdir initrd
mount -o loop initrd-img initrd/

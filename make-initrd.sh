#!/bin/bash
### modify initrd by adding the scripts install-scarlet.sh
### and install-wan.sh

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)
path=$(pwd)

### copy the original initrd.img, unzip and mount it
cp slack-cd1/isolinux/initrd.img initrd-img.gz
gunzip initrd-img.gz
rm -rf initrd/
mkdir initrd/
mount -o loop initrd-img initrd/

### modify initrd by adding install-scarlet.sh and install-wan.sh
cp install-scarlet.sh initrd/
cp install-wan.sh initrd/
chown root.root initrd/install-scarlet.sh
chown root.root initrd/install-wan.sh

### copy initrd to scarlet-cd
umount initrd/
gzip initrd-img
rm -rf scarlet-cd/
mkdir -p scarlet-cd/isolinux/
mv initrd-img.gz scarlet-cd/isolinux/initrd.img

### clean
rmdir initrd


#!/bin/bash

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### unmount and clean
umount initrd/
rmdir initrd
rm initrd-img

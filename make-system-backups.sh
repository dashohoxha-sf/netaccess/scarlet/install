#!/bin/bash
### make backups of the partitions hda2 of images/wan.img, 
### hda2 of images/scarlet.img and hda5 of images/scarlet.img

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to the image/ directory
cd $(dirname $0)
cd images/

### mount the second partition of wan.img to the directory wan.hda2/
/sbin/losetup -o 271434240 /dev/loop0 wan.img
mkdir -p wan.hda2/
mount /dev/loop0 wan.hda2/

### mount the second partition of scarlet.img to the directory scarlet.hda2/
/sbin/losetup -o 271434240 /dev/loop1 scarlet.img
mkdir -p scarlet.hda2/
mount /dev/loop1 scarlet.hda2/

### mount the partition 5 of scarlet.img to the directory scarlet.hda5/
/sbin/losetup -o 806109696 /dev/loop2 scarlet.img
mkdir -p scarlet.hda5/
mount /dev/loop2 scarlet.hda5/

### make backups of wan.hda2/, scarlet.hda2/ and scarlet.hda5/
tar --create --gzip --preserve --one-file-system --verbose \
    --file=wan.hda2.tar.gz     --directory=wan.hda2/  .
tar --create --gzip --preserve --one-file-system --verbose \
    --file=scarlet.hda2.tar.gz --directory=scarlet.hda2/  .
tar --create --gzip --preserve --one-file-system --verbose \
    --file=scarlet.hda5.tar.gz --directory=scarlet.hda5/  .

### clean up
umount wan.hda2/
umount scarlet.hda2/
umount scarlet.hda5/
/sbin/losetup -d /dev/loop0
/sbin/losetup -d /dev/loop1
/sbin/losetup -d /dev/loop2
rmdir wan.hda2/
rmdir scarlet.hda2/
rmdir scarlet.hda5/


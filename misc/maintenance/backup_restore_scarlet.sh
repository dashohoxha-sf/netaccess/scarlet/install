
function backup_scarlet()
{
  select_system
  if [ "$SYSTEM" = "" ]
  then
    $DIALOG --backtitle "Scarlet Maintenance" \
            --msgbox "No scarlet selected. Operation canceled." 0 0
    return 1
  fi

  while [ "$BACKUP_MEDIA" = "" ] ; do select_backup_media ; done

  if [ "$BACKUP_MEDIA" = "network" ]
  then
    configure_network
    get_backupserver_params
    backup_to_network
  else
    get_harddisk_params
    backup_to_harddisk
  fi
}

function restore_scarlet()
{
  select_system
  if [ "$SYSTEM" = "" ]
  then
    $DIALOG --backtitle "Scarlet Maintenance" \
            --msgbox "No scarlet selected. Operation canceled." 0 0
    return 1
  fi

  while [ "$BACKUP_MEDIA" = "" ] ; do select_backup_media ; done

  if [ "$BACKUP_MEDIA" = "network" ]
  then
    configure_network
    get_backupserver_params
    restore_from_network
  else
    get_harddisk_params
    restore_from_harddisk
  fi
}

function select_backup_media()
{
  BACKUP_MEDIA=""

  $DIALOG --backtitle "Scarlet Maintenance" \
          --title "Backup Media" \
          --radiolist "" 8 20 2 \
          "network" "" on "harddisk" "" off 2> $tempfile

  retval=$?
  case $retval in
    0)   BACKUP_MEDIA=$(cat $tempfile) ;;
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac
}

### get the values of the variables IP and GATEWAY, which are used 
### for the configuration of the interface eth0 of the router
function configure_network()
{
  ip=${IP:-192.168.0.3/24}
  gateway=${GATEWAY:-192.168.0.1}

  $DIALOG --backtitle "Scarlet Maintenance" \
          --title "Configure Network" \
          --form "Configure the interface eth0 of the router \
so that it can access the backup server:" 11 40 0 \
          "IP Address:" 1 1 "$ip"       1 13 20 0 \
          "Gateway:"    2 1 "$gateway"  2 13 20 0 \
          2> $tempfile

  retval=$?
  case $retval in
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac

  ### get the values into the variables IP and GATEWAY
  cp $tempfile network_params
  sed -e '1 s/^/IP=/' -i network_params
  sed -e '2 s/^/GATEWAY=/' -i network_params
  . network_params

  ### (re)configure the network
  /sbin/ip link set eth0 up
  /sbin/ip address flush dev eth0
  /sbin/ip address add $IP dev eth0
  if [ "$GATEWAY" != "" ]
  then
    /sbin/ip route add to default via $GATEWAY dev eth0
  fi
}

### get the values of the backup server parameters, which are used
### to access the backup file (for reading or writing) by samba 
### (Windows network shares); these parameters are saved in the global
### variables: BS_IP, BS_SHARE, BS_USERNAME, BS_PASSWORD and BS_DOMAIN
function get_backupserver_params()
{
  bs_ip=${BS_IP:-192.168.0.100}
  bs_share=${BS_SHARE:-scarlet_backups}

  $DIALOG --backtitle "Scarlet Maintenance" \
          --title "Backup Server" \
          --form "Give the IP of the backup server and the name \
of the shared folder. Username, password and the domain can be left empty, \
if they are not needed." \
          16 40 0 \
          "Server IP:" 1 1 "$bs_ip"       1 11 20 0 \
          "Sharename:" 2 1 "$bs_share"    2 11 20 0 \
          "Username:"  3 1 "$BS_USERNAME" 3 11 20 0 \
          "Password:"  4 1 "$BS_PASSWORD" 4 11 20 0 \
          "Domain:"    5 1 "$BS_DOMAIN"   5 11 20 0 \
          2> $tempfile

  retval=$?
  case $retval in
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac

  ### get the values into the variables
  cp $tempfile backup_server_params
  sed -e '1 s/^/BS_IP=/' -i backup_server_params
  sed -e '2 s/^/BS_SHARE=/' -i backup_server_params
  sed -e '3 s/^/BS_USERNAME=/' -i backup_server_params
  sed -e '4 s/^/BS_PASSWORD=/' -i backup_server_params
  sed -e '5 s/^/BS_DOMAIN=/' -i backup_server_params
  . backup_server_params
}

function backup_to_network()
{
  ### get the partition to be backup-ed
  if [ "$SYSTEM" = "scarlet-1" ]
  then
    local partition=/dev/hda5
  else
    local partition=/dev/hda6
  fi

  ### get the name of the backup file
  date=$(date '+%Y%m%d')
  backup_file="$SYSTEM-$date.img.gz"

  ### create pipe files
  rm -f system.img
  rm -f system.img.gz
  mkfifo system.img
  mkfifo system.img.gz

  ### send the content of the partition to the pipe file 'system.img.gz'
  dd if=$partition 2>dd_progress >system.img &
  dd_pid=$!
  gzip --stdout <system.img  >system.img.gz &

  ### create an authentication file for smbclient
  cat <<EOF > smb_authentication.txt
username = $BS_USERNAME
password = $BS_PASSWORD
domain = $BS_DOMAIN
EOF

  ### save the data coming from the pipe file
  ### to a remote file in the backup server
  smbclient //$BS_IP/$BS_SHARE \
            --authentication-file=smb_authentication.txt \
            --command "put system.img.gz $backup_file" &>/dev/null &

  ### display the copy progress in a gauge
  let totalbytes="128 * 8225280"    # size of the partition is 128 cylinders
  message="Copying $SYSTEM to //$BS_IP/$BS_SHARE/"
  display_copy_progress $dd_pid dd_progress $totalbytes "$message"

  ### cleanup
  rm system.img
  rm system.img.gz
  rm smb_authentication.txt
}


function restore_from_network()
{
  ### get the partition to be restored-ed
  if [ "$SYSTEM" = "scarlet-1" ]
  then
    partition=/dev/hda5
  else
    partition=/dev/hda6
  fi

  ### get the name of the backup file
  date=$(date '+%Y%m%d')
  backup_file="$SYSTEM-$date.img.gz"
  $DIALOG --backtitle "Scarlet Maintenance" \
          --title "Backup File" \
          --inputbox "Enter the name of the backup file:" \
          8 45 $backup_file 2> $tempfile
  retval=$
  case $retval in
    1)   return 1   ;;   # Cancel pressed
    255) return 255 ;;   # ESC pressed
  esac
  backup_file=$(cat $tempfile)

  ### create pipe files
  rm -f system.img
  rm -f system.img.gz
  mkfifo system.img
  mkfifo system.img.gz

  ### create an authentication file for smbclient
  cat <<EOF > smb_authentication.txt
username = $BS_USERNAME
password = $BS_PASSWORD
domain = $BS_DOMAIN
EOF

  ### save the data coming from the pipe file
  ### to a remote file in the backup server
  smbclient //$BS_IP/$BS_SHARE \
            --authentication-file=smb_authentication.txt \
            --command "get $backup_file system.img.gz" &>/dev/null &

  ### send the content of the partition to the pipe file 'system.img.gz'
  gunzip --stdout  <system.img.gz >system.img &
  dd of=$partition 2>dd_progress <system.img  & 
  dd_pid=$!

  ### display the copy progress in a gauge
  let totalbytes="128 * 8225280"    # size of the partition is 128 cylinders
  message="Restoring $backup_file from //$BS_IP/$BS_SHARE/ to $SYSTEM"
  display_copy_progress $dd_pid dd_progress $totalbytes "$message"

  ### cleanup
  rm system.img
  rm system.img.gz
  rm smb_authentication.txt

  ### modify the /etc/fstab of the destination system
  umount /mnt/tmp 2>/dev/null
  mount $partition /mnt/tmp
  sed -e "\+/dev/hda5+ s+/dev/hda5+$partition+g" -i /mnt/tmp/etc/fstab
  sed -e "\+/dev/hda6+ s+/dev/hda6+$partition+g" -i /mnt/tmp/etc/fstab
  umount /mnt/tmp
}

function backup_to_harddisk()
{
  echo ""
}

function restore_from_harddisk()
{
  echo ""
}

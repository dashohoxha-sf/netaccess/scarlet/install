
##########################################################################
### This function displays the progress of the 'dd' process in a gauge.
###
### The copy ('dd') command should be called like this: 
###   dd if=/dev/xxx of=/dev/yyy  2>$progress_file &
###   dd_pid=$!
###
### Expects 4 parameters:
###   $dd_pid        -- the id of the 'dd' proccess
###   $progress_file -- the file where 'stderr' of 'dd' is redirected
###   $totalbytes    -- the size in bytes of the thing that is being copied
###   $message       -- the message that is displayed in the gauge
##########################################################################

function display_copy_progress()
{
  ### get the parameters
  local dd_pid=$1 ; shift
  local dd_progress=$1 ; shift
  local totalbytes=$1 ; shift
  local message=""
  until test -z "$1"; do message="$message $1"; shift; done

  ### declare some local variables
  local percentage=0
  local infoline=""
  local bytes=0

  ( 
    while test $percentage != 100
    do
      ### wait 5 sec
      sleep 5

      ### query the dd process for the progress
      kill -USR1 $dd_pid || break
      read infoline ; read infoline ; read infoline
      : > $dd_progress
      
      ### get bytes copied up to now
      bytes=$(echo $infoline | cut -d' ' -f1)
      if test -z "$bytes"; then continue; fi

      ### calculate the % copied
      let percentage="100 * $bytes / $totalbytes"

      ### modify infoline to make it more suitable for display
      infoline=${infoline#*(}
      infoline=${infoline/)/}
      infoline=${infoline/.* seconds/ sec}
      if test -z "$infoline"; then continue; fi

      ### display in the gauge the percentage, the message
      ### and some info about the progress of the copy process
      echo "XXX"
      echo $percentage
      echo "$message \n $infoline"
      echo "XXX"
    done < $dd_progress
  ) | $DIALOG --backtitle "Scarlet Maintenance" \
              --title "Copy Progress" \
              --gauge "$message:" 8 60 0

  ### make sure that copying is finished totally
  local process_line=$(ps ax | grep $dd_pid | grep -v grep)
  until test -z "$process_line"
  do
    sleep 1
    process_line=$(ps ax | grep $dd_pid | grep -v grep)
  done

  ### clean up
  rm $dd_progress
}

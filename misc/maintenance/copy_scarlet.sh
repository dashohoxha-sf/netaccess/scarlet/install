
function copy_scarlet()
{
  ### get the direction of copy (1->2 or 2->1)
  local direction=""

  $DIALOG --backtitle "Scarlet Maintenance" --title " Copy " \
      --radiolist "This will make an identical physical copy \
of one scarlet to the other. \n\
Make sure that you select the right direction of copy!\n\
(press spacebar to select)" \
      13 60 2 \
      "1->2" "Copy scarlet-1 to scarlet-2" off \
      "2->1" "Copy scarlet-2 to scarlet-1" off \
      2> $tempfile

  retval=$?
  case $retval in
    0)   direction=$(cat $tempfile) ;;
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac

  ### get the source and destination variables
  if [ "$direction" = "1->2" ]
  then
    src_system="scarlet-1"
    src_partition=/dev/hda5
    dst_system="scarlet-2"
    dst_partition=/dev/hda6
  elif [ "$direction" = "2->1" ]
  then
    src_system="scarlet-2"
    src_partition=/dev/hda6
    dst_system="scarlet-1"
    dst_partition=/dev/hda5
  else
    $DIALOG --backtitle "Scarlet Maintenance" \
            --msgbox "No choice made. \nOperation canceled." 0 0
    return 1
  fi

  ### make sure that the selected copy direction is right
  $DIALOG --backtitle "Scarlet Maintenance" \
      --yesno "Are you sure you want to copy \n\
$src_system to $dst_system ?" 0 0
   case $? in
    1)   return 1 ;; # No chosen
    255) return 1 ;; # ESC pressed
  esac

  ### start the copy process
  dd if=$src_partition of=$dst_partition 2>dd_progress &
  dd_pid=$!

  ### display the copy progress in a gauge
  let totalbytes="128 * 8225280"    # size of the partition is 128 cylinders
  message="Copying $src_system to $dst_system:"
  display_copy_progress $dd_pid dd_progress $totalbytes "$message"

  ### modify the /etc/fstab of the destination system
  umount /mnt/tmp 2> /dev/null
  mount $dst_partition /mnt/tmp
  sed -e "\+$src_partition+ s+$src_partition+$dst_partition+g" \
      -i /mnt/tmp/etc/fstab
  umount /mnt/tmp
}

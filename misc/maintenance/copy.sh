#!/bin/bash

: ${DIALOG=dialog}

### include params
. selected_values

### include function display_copy_progress
. display_copy_progress.sh

partition=/dev/hda6

### get the name of the backup file
date=$(date '+%Y%m%d')
backup_file="$SYSTEM-$date.img.gz"

### create pipe files
rm -f system.img
rm -f system.img.gz
rm -f dd_progress
mkfifo system.img
mkfifo system.img.gz
#mkfifo dd_progress

### send the content of the partition to the pipe file 'system.img.gz'
dd if=$partition > system.img 2> dd_progress &
dd_pid=$!
echo $dd_pid
gzip --stdout < system.img  > system.img.gz &

### save the data coming from the pipe file
### to a remote file in the backup server
smbclient //$BS_IP/$BS_SHARE \
          --authentication-file=smb_authentication.txt \
          --command "put system.img.gz $backup_file" &>debug.txt &


let totalbytes="128 * 8225280"    # size of the partition is 128 cylinders
message="Copying $SYSTEM to //$BS_IP/$BS_SHARE/"
display_copy_progress $dd_pid dd_progress $totalbytes $message

#!/bin/bash
### present to the user a menu for maintaining scarlet

: ${DIALOG=dialog}

tempfile=`tempfile 2>/dev/null` || tempfile=/tmp/test$$
trap "rm -f $tempfile" 0 1 2 5 15

### go to this directory
cd $(dirname $0)

### include some previously selected values
. selected_values

### include the functions
. display_copy_progress.sh
. copy_scarlet.sh
. backup_restore_scarlet.sh

### display the main menu from where the user can select an operation
### the selection is saved to the global variable $OPERATION
function mainmenu()
{
  $DIALOG --backtitle "Scarlet Maintenance" \
    --title "Select Operation" \
    --default-item $OPERATION \
    --menu "Use the UP/DOWN arrow keys, the first letter of the choice \
as a hot key, or the number keys 1-6 to choose an option.\n\n\
Choose the operation you would like to perform:" \
    18 61 6 \
    "Help"     "Information about what can be done and how" \
    "Copy"     "Copy one scarlet to the other" \
    "Booting"  "Select the scarlet to boot by default" \
    "Backup"   "Backup scarlet to disk or to network" \
    "Restore"  "Restore scarlet from a disk or network backup" \
    "Exit"     "Exit from the maintenance system" \
    "Manual"   "Drop to command line for manual maintenance" \
    2> $tempfile

  retval=$?
  case $retval in
    0)   choice=$(cat $tempfile) ;;
    1)   choice=Exit ;;  # Cancel pressed
    255) choice=Exit ;;  # ESC pressed
  esac

  OPERATION=$choice
}

function display_help()
{
  $DIALOG --no-shadow --backtitle "Scarlet Maintenance" \
          --title " Help " --textbox "help" 20 78
}

function go_to_commandline()
{
  $DIALOG --clear
  exit 0
}

### select which one to boot by default, scarlet-1 or scarlet-2
function booting_scarlet()
{
  ### select scarlet-1 or scarlet-2
  select_system
  if [ "$SYSTEM" = "" ]
  then
    $DIALOG --backtitle "Scarlet Maintenance" \
            --msgbox "No scarlet selected. Operation canceled." 0 0
    return 1
  fi

  ### get the (lilo) name of the selected system
  if [ "$SYSTEM" = "scarlet-1" ]
  then
    default="Scarlet1"
  else
    default="Scarlet2"
  fi

  ### modify the default in lilo.conf and reinstall lilo
  sed -e "/default/ d" -i /etc/lilo.conf
  sed -e "/timeout/ a default = $default" -i /etc/lilo.conf
  lilo
}

### select a system (either scarlet-1 or scarlet-2)
### and save the selection in the global variable $SYSTEM
function select_system()
{
  SYSTEM=""

  $DIALOG --backtitle "Scarlet Maintenance" \
          --radiolist "Which scarlet?" 9 20 2 \
          "scarlet-1" "" off "scarlet-2" "" off 2> $tempfile

  retval=$?
  case $retval in
    0)   SYSTEM=$(cat $tempfile) ;;
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac
}

### give a choice to reboot or shutdown (or to cancel,
### in case that Exit was selected by mistake
function exit_maintenance()
{
  save_variables

  local choice=Reboot

  $DIALOG --backtitle "Scarlet Maintenance" --title " Exit " \
    --menu "Reboot or shutdown the router? \n (<Cancel> or ESC to cancel)" \
    10 35 2 \
    "Reboot" "Reboot router" \
    "Shutdown" "Shutdown router" \
    2> $tempfile

  retval=$?
  case $retval in
    0)   choice=$(cat $tempfile) ;;
    1)   return 1   ;;  # Cancel pressed
    255) return 255 ;;  # ESC pressed
  esac

  $DIALOG --clear

  if [ "$choice" = "Reboot" ]
  then reboot
  elif [ "$choice" = "Shutdown" ]
  then halt
  fi

  exit 0
}

### save the selected values, in order to reuse them next time
function save_variables()
{
  cat <<EOF > selected_values
SYSTEM="$SYSTEM"
BACKUP_MEDIA="$BACKUP_MEDIA"
IP="$IP"
GATEWAY="$GATEWAY"
BS_IP="$BS_IP"
BS_SHARE="$BS_SHARE"
BS_USERNAME="$BS_USERNAME"
BS_PASSWORD="$BS_PASSWORD"
BS_DOMAIN="$BS_DOMAIN"
EOF
}

### call the mainmenu function in an endless loop and then 
### call another function according to the selected operation
OPERATION=Help
while true
do
  mainmenu
  case $OPERATION in
    Help)    display_help ;;
    Copy)    copy_scarlet ;;
    Backup)  backup_scarlet ;;
    Restore) restore_scarlet ;;
    Manual)  go_to_commandline ;;
    Booting) booting_scarlet ;;
    Exit)    exit_maintenance ;;
  esac
done

#!/bin/bash
### make accessible partitions on netaccess.img and testbox.img

### go to this directory
cd $(dirname $0)

### partitions on netaccess.img
# /dev/hda1 (swap)
/sbin/losetup -o 32256      /dev/loop0 ../images/netaccess.img     
# /dev/hda2 (maintenance)
/sbin/losetup -o 271434240  /dev/loop1 ../images/netaccess.img
# /dev/hda5 (scarlet-1)
/sbin/losetup -o 806109696  /dev/loop2 ../images/netaccess.img
# /dev/hda6 (scarlet-2)
/sbin/losetup -o 1867170816 /dev/loop3 ../images/netaccess.img
# /dev/hda7 (squid-cache)
/sbin/losetup -o 2928231936 /dev/loop4 ../images/netaccess.img

### partitions on testbox.img
#/sbin/losetup -o 131604480 /dev/loop5 ../images/testbox.img
#/sbin/losetup -o 386588160 /dev/loop6 ../images/testbox.img

### now they can be used like this
# dd if=/dev/loop3 of=/dev/loop0
# dd if=/dev/loop4 of=/dev/loop1
# dd if=/dev/loop1 of=/dev/loop2
# etc.


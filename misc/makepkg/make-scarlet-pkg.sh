#!/bin/bash

if [ "$1" = "" ]
then
  echo "Usage: $0 release-nr"
  exit 1
fi
nr=$1

### go to this directory
cd $(dirname $0)

### get the latest version of scarlet
rm -rf var/
mkdir -p var/www/htdocs/
cp -a ../scarlet var/www/htdocs/

makepkg scarlet-$nr.tgz


#!/bin/sh
### Make the ISO image of the installation CD. This CD contains
### the backups scarlet.hda2.tar.gz, scarlet.hda5.tar.gz and
### wan.hda2.tar.gz. It also contains the installation scripts
### install-scarlet.sh and install-wan.sh .
### This CD is a Slackware bootable CD, without any slackware packages.
### After it is booted, the script install-scarlet.sh or the script
### install-wan.sh can be used to install the systems scarlet or wan.

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### make initrd.img
./make-initrd.sh

### make backups of scarlet.hda2, scarlet.hda5 and wan.hda2
./make-system-backups.sh

### create the ISO image
mkisofs -R -J -v -d -N -hide-rr-moved -graft-points \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -sort slack-cd1/isolinux/iso.sort \
        -b isolinux/isolinux.bin \
        -c isolinux/isolinux.boot \
        -V "Scarlet Install" \
        -A "Scarlet Install CD" \
        -o images/scarlet.iso \
        -x slack-cd1/isolinux/initrd.img \
        -x slack-cd1/slackware \
        slack-cd1/ \
        scarlet-cd/ \
        /=images/scarlet.hda2.tar.gz \
        /=images/scarlet.hda5.tar.gz \
        /=images/wan.hda2.tar.gz \
        /=install-scarlet.sh \
        /=install-wan.sh

### clean up
rm -rf scarlet-cd/


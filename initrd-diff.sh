#!/bin/bash
### make a patch for the setup scripts of initrd

### make sure that the script is called by root
if [ $(whoami) != "root" ]; then echo "Should be called by root"; exit; fi

### go to this directory
cd $(dirname $0)

### mount
./initrd-mount.sh slack-cd1/isolinux/initrd.img

### make the patch
diff -rubB initrd/usr/lib/setup/ router-setup/ > initrd-setup-patch.diff

### clean
./initrd-clean.sh

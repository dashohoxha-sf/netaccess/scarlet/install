#!/bin/sh
### install netaccess

### create the partition table of /dev/hda
echo "Erasing the current partition table of /dev/hda"
dd if=/dev/zero of=/dev/hda count=63b
echo "Creating a new partition table for /dev/hda"
fdisk_commands="
n\n p\n 1\n \n +32\n \
n\n p\n 2\n \n \n \
t\n 1\n 82\n \
w\n"
#echo -e $fdisk_commands  ##debug
echo -e $fdisk_commands | fdisk /dev/hda
fdisk -l /dev/hda

### mount cdrom
mkdir /mnt/cdrom
mount /dev/hdc /mnt/cdrom

### install the maintenance system in /dev/hda2
echo "Formating /dev/hda2:"
mkreiserfs /dev/hda2
echo "Installing the system:"
mkdir /hda2
mount /dev/hda2 /hda2
cd /hda2
tar --extract --gunzip --preserve --file=/mnt/cdrom/wan.hda2.tar.gz -v
cd /
echo "Installing lilo:"
chroot /hda2 lilo

### format the swap partition
echo "Formating the swap partition:"
mkswap /dev/hda1


#!/bin/bash
### Emulate a system which boots from the installation CD and has a blank
### harddisk scarlet.img. After the system is booted, the installation
### should be done with the command `install-scarlet.sh`.
### Then the installed system can be tested like this:
###     # ../testing/test.sh images/scarlet.img

### go to this directory
cd $(dirname $0)

### get parameters
image=${1:-images/scarlet.img}
size=${2:-2GB}

### create the image if it doesn't exist
if [ ! -f $image ]; then qemu-img create $image $size; fi

### install scarlet from the image
qemu -kernel-kqemu -boot d -cdrom images/scarlet.iso $image

### testing the installed system
echo "To test the installed system run the command:"
echo "../testing/test.sh $image"

